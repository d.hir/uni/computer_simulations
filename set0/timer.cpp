#include "timer.h"

Timer::Timer (double timedelta_) {
  this->timedelta = timedelta_;
};

void Timer::start() {
  this->startpoint = std::chrono::steady_clock::now();
};

void Timer::end() {
  this->endpoint = std::chrono::steady_clock::now();
};

double Timer::get() const {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(this->endpoint - this->startpoint).count();
};

std::ostream& operator<<(std::ostream& stream, const Timer& timer) {
  double duration = timer.timedelta;
  if (timer.timedelta == 0)
    duration = timer.get();
  if (duration < 1e3)
    stream << duration << " ns";
  else if (duration < 1e6)
    stream << duration / 1e3 << " us";
  else if (duration < 1e9)
    stream << duration / 1e6 << " ms";
  else
    stream << duration / 1e9 << " s";
  return stream;
};
