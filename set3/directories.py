import time
import numpy
import os

def makedirs(arglist):
  foldername = str(int(numpy.round(time.time())));
  os.makedirs(foldername);
  names = [os.path.join(foldername, v) for v in ["4_1", "4_2", "5_1", "5_2"]];
  [os.makedirs(n) for n in names];
  argliststr = [[os.path.join(n, 'L' + "{:02d}".format(r[0]) + '-beta' + 
      '_'.join("{:1.2f}".format(r[1]).split('.'))) for r in ex] 
      for n, ex in zip(names, arglist)];
  [[os.makedirs(r) for r in a] for a in argliststr];
  v = [];
  for a in argliststr:
    v += a;
  return v;
