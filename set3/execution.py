import multiprocessing
from ising import *
from directories import makedirs

def doSimulation(L, beta, nequi, fname, nmeas = 10000, nskip = 0, ):
  m = Ising(beta, L);
  m.logData = True;
  m.simulate(fname, nmeas, nskip, nequi);
  del m;

def execute(mode = 'parallel'):
  beta_C = 0.44068;
  therm = 10;
  arg41 = [[4, 0.2, 0],     [4, 0.4, 0],     [4, 0.6, 0],
           [6, 0.2, therm], [6, 0.4, therm], [6, 0.6, therm], 
           [8, 0.2, therm], [8, 0.4, therm], [8, 0.6, therm]];
  arg42 = [[4,  beta_C, therm], [8,  beta_C, therm], 
           [16, beta_C, therm], [32, beta_C, therm]];
  arg51 = [[8,  0.2, therm], [8,  beta_C, therm], [8,  0.6, therm],
           [32, 0.2, therm], [32, beta_C, therm], [32, 0.6, therm]];
  args = arg41 + arg42 + arg51;
  foldernames = makedirs([arg41, arg42, arg51]);
  argtot = [a + [f] for a, f in zip(args, foldernames)];
  
  if mode == 'parallel':
    p = multiprocessing.Pool(multiprocessing.cpu_count());
    p.starmap(doSimulation, argtot);
    p.close();
  else:
    for a in argtot:
      doSimulation(*a);
