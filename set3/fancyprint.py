import numpy

def fancyprint(S):
  j = int(0);
  L = numpy.sqrt(len(S));
  for s in S:
    print('%+d' % int(s), end = ' ');
    j += 1;
    if (j % L) == 0:
      print();
