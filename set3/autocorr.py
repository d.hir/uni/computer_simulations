import numpy as np
from scipy import fftpack
import matplotlib.pyplot as mpl

INVALID_CORRELATION_TIME = 1;

def autocorr(x_t, t_max):

  # length of autocorrelation function
  N = len(x_t)
  t_max = min(t_max, N);

  # predefinition of arrays
  B_t = np.zeros(N)
  C_t = np.zeros(N)
  S1_t = np.zeros(N)
  S2_t = np.zeros(N)

  # starting values before loop
  B_t[N-1] = x_t[0]
  C_t[N-1] = x_t[N-1]
  S1_t[N-1] = x_t[0]**2
  S2_t[N-1] = x_t[N-1]**2

  # calculation of the first 4 sums
  for t in range(N-1) :
      B_t[-2-t] = B_t[-1-t] + x_t[t+1]
      C_t[-2-t] = C_t[-1-t] + x_t[-2-t]
      S1_t[-2-t] = S1_t[-1-t] + x_t[t+1]**2
      S2_t[-2-t] = S2_t[-1-t] + x_t[-2-t]**2

  # calculation of parameter A(t)
  nextpow2 = np.ceil(np.log2(len(x_t)))
  A_t_temp = fftpack.fft(x_t, int(2**(nextpow2+1)))
  A_t_fft = fftpack.ifft(A_t_temp * A_t_temp.conjugate())
  A_t= np.real(A_t_fft[:N])

  # Calculate it with the retrieved formula from set2
  Nt = 1 / (N - np.linspace(0, t_max-1, t_max))
  rho_e_t = (A_t[:t_max] - Nt*B_t[:t_max]*C_t[:t_max]) / np.sqrt((S1_t[:t_max] -
            Nt * B_t[:t_max]**2) * (S2_t[:t_max] - Nt*C_t[:t_max]**2))

  #return function output
  return rho_e_t

def calcCorrelation(x, t_max, t):
  rho = autocorr(x, t_max);
  p = np.polyfit(t[rho > 0], np.log(np.abs(rho[rho > 0])), 1);
  pt = np.exp(np.polyval(p, t));
  tau = - 1 / p[0];
  if tau < 0: tau = INVALID_CORRELATION_TIME;
  return rho, pt, tau;

def evaluateCorrelations(m, t_max = 100):
  t = np.linspace(0, t_max - 1, t_max);
  rho_E, p_E, tau_E = calcCorrelation(m.E, t_max, t);
  rho_M, p_M, tau_M = calcCorrelation(m.M, t_max, t);

  fig, (ax_E, ax_M) = mpl.subplots(1, 2, figsize=(12, 6));
  ax_E.semilogy(t[rho_E > 0], rho_E[rho_E > 0]);
  ax_M.semilogy(t[rho_M > 0], rho_M[rho_M > 0]);
  ax_E.semilogy(t[rho_E > 0], p_E[rho_E > 0]);
  ax_M.semilogy(t[rho_M > 0], p_M[rho_M > 0]);
  ax_E.set_xlabel('t / sweeps');
  ax_M.set_xlabel('t / sweeps');
  ax_E.set_ylabel('C(t) / 1');
  ax_M.set_ylabel('C(t) / 1');
  ax_E.set_title('C(t) for energy');
  ax_M.set_title('C(t) for magnetization');
  fig.suptitle('Autocorrelations C(t)');

  return tau_E, tau_M, fig;
