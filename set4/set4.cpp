#include "travellingsalesman.h"
#include <iostream>
#include <string>

//----------------------------------------------------------------------------//

int main (int argc, char** argv) {
  TravellingSalesman* tsm;
  if (argc == 2) 
    tsm = new TravellingSalesman (vectorutils::load_csv<double>
        (std::string(argv[1])), 1.0);
  else
    tsm = new TravellingSalesman (512, 1, 17);
  tsm->setUpdatesPerBeta(10000);
  TravellingSalesman::setMetric(Metric::euclidean);
  tsm->printTowns();
  tsm->plotLandscape("Landscape / Map");
  tsm->simulate(std::vector<KVals> {{1, 10}, {5, 15}, {10, 50}});
  tsm->plotLandscape("Shortest found path", true, true);
  
  return 0;
}
