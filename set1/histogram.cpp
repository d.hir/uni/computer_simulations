#include "histogram.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <boost/tuple/tuple.hpp>
#include <string>
#include <boost/format.hpp>
#include "./gnuplot-iostream.h"


Histogram::Histogram(int num_bins_, double min_, double max_) {
  this->num_bins = num_bins_;
  this->min = min_;
  this->max = max_;
  this->range = this->max - this->min;
  this->bin_width = *(new 
      std::vector<double>(this->num_bins, this->range / this->num_bins));
  this->bin = *(new std::vector<int> (this->num_bins, 0));
  this->total_elements = 0;
  this->remainder_bin = 0;
};

int Histogram::setIndividualWidth(std::vector<double> bin_width_) {
  double sum = 0;
  for (int j = 0; j < this->num_bins; j++)
    sum += bin_width_[j];
  if (sum > this->range * (1 + TOLERANCE) or 
      sum < this->range * (1 - TOLERANCE)) {
    std::cerr << "HISTOGRAM: BIN-WIDTH DOES NOT MATCH PREVIOUS SETTINGS!"
        << std::endl;
    return 1;
  } else {
    this->bin_width = bin_width_;
    return 0;
  }
};

void Histogram::setMaxVals() {
  this->maxvals = *(new std::vector<double> (this->num_bins));
  this->maxvals[0] = this->min + this->bin_width[0];
  for (int j = 1; j < this->num_bins; j++)
    this->maxvals[j] = this->maxvals[j - 1] + this->bin_width[j];
};

void Histogram::setData(std::vector<double> data) {
  Histogram::setMaxVals();
  for (auto d : data) {
    this->total_elements++;
    if (d < this->min || d > this->max) {
      this->remainder_bin++;
      continue;
    }
    for (int j = 0; j < this->num_bins; j++) {
      if (d < maxvals[j]) {
        this->bin[j]++;
        break;
      }
    }  
  }
};

std::vector<std::string> Histogram::getXTicks(int num_entries) {  
  int bins_per_entry = (this->num_bins - 1) / (num_entries - 1);
  
  std::vector<std::string> xticks;
  for (int j = 1; j < num_bins; j += bins_per_entry) {
    xticks.push_back(str(boost::format("\"%.2f\" %d") 
        % this->maxvals[j - 1] % j));
  }
  return xticks;
};

void Histogram::plot(int variant) {
  Gnuplot gp;
  std::vector<double> uncert;
  std::vector<double> plotbins (this->num_bins);
  std::string plottitle;
  switch (variant) {
    case 0:
      gp << "plot '-' with boxes title 'Histogram bars'\n";
      gp.send1d(this->bin);
      return;
    case 1:
      uncert = Histogram::frequentistUncertainty();
      for (int j = 0; j < this->num_bins; j++)
        plotbins[j] = (double) this->bin[j];
      plottitle = "Frequentist Analysis: Not normalized";
      break;
    case 2:
      uncert = Histogram::frequentistUncertainty();
      for (int j = 0; j < this->num_bins; j++) {
        plotbins[j] = (double) this->bin[j] / this->total_elements;
        uncert[j] /= this->total_elements;
      }
      plottitle = "Frequentist Analysis: normalized";
      break;
    case 3:
      for (int j = 0; j < this->num_bins; j++)
        plotbins[j] = ((double) this->bin[j] + 1) / 
            (this->total_elements + this->num_bins + 1);
      uncert = Histogram::bayesianUncertainty(plotbins);
      plottitle = "Bayesian Analysis";
      break;
    default:
      std::cout << "Unrecognized plotting variant: " << variant << std::endl;
      return;
  }
  gp << "set style histogram errorbars gap 0 lw 1\n";
  gp << "set style data histogram\n";
  gp << "set title \"" << plottitle << "\"\n";
  std::vector<std::string> xticks = Histogram::getXTicks(5);
  gp << "set xrange [0:" << this->num_bins << "]\n";
  gp << "set xtics (";
  for (auto tic : xticks)
    gp << tic << ",";
  gp << ")\n";
  gp << "plot '-' using 1:2 title 'Histogram bars'\n";

  gp.send1d(boost::make_tuple(plotbins, uncert));
};

std::vector<double> Histogram::frequentistUncertainty() {
  std::vector<double> uncert (this->num_bins);
  for (int b = 0; b < this->num_bins; b++) {
    uncert[b] = sqrt(this->bin[b] * (1 - this->bin[b] / this->total_elements));
  }
  return uncert;
};

std::vector<double> Histogram::bayesianUncertainty(std::vector<double> avg_pi){
  std::vector<double> uncert (this->num_bins);
  for (int b = 0; b < this->num_bins; b++)
    uncert[b] = sqrt(avg_pi[b] * (1 - avg_pi[b]) / 
        (this->total_elements + this->num_bins + 2));
  return uncert;
};

void Histogram::printBins() {
  std::cout << "Remainder bin elements: " << this->remainder_bin << std::endl;
  std::cout << this->min << " to " << this->maxvals[0] << ": " << this->bin[0]
      << std::endl;
  for (int j = 0; j < this->num_bins - 1; j++)
    std::cout << this->maxvals[j] << " to " << this->maxvals[j + 1] << ": "
        << this->bin[j + 1] << std::endl;
};
