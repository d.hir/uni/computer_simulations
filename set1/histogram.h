#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <vector>
#include "./gnuplot-iostream.h"

#define TOLERANCE 0.01

class Histogram {
  private:
    int num_bins;
    std::vector<double> bin_width;
    std::vector<int> bin;
    double min;
    double max;
    int remainder_bin;
    Gnuplot gp;
    double range;
    std::vector<double> maxvals;
    int total_elements;
    
    void setMaxVals();
    std::vector<double> frequentistUncertainty();
    std::vector<double> bayesianUncertainty(std::vector<double> avg_pi);
    std::vector<std::string> getXTicks(int num_ticks);
    
  public:
    Histogram(int num_bins_, double min_, double max_);
    int setIndividualWidth(std::vector<double> bin_width_);
    void setData(std::vector<double> data);
    void plot(int variant);
    void printBins();

};

#endif
