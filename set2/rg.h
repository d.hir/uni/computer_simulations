///////////////////////////////////////
/// RG.H
/// ====
/// CLASS CREATING (BETTER) RANDOM NUMBERS
/// PROVIDES EASY TO USE FRONTEND FOR STANDARD C++ <RANDOM>
//////////////////////////////////////

#ifndef RG_H
#define RG_H

/* provides random numbers */
#include <random>
#include <cmath>

class RandomGenerator {
  private:
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> disint;
    std::uniform_int_distribution<long> dislong;
    std::uniform_real_distribution<> disreal;
    
  public:
    /* standard constructor; not recommended as this does not setup the distributions */
    RandomGenerator();
    ~RandomGenerator();
    /* recommended constructor */
    RandomGenerator(int minint, int maxint, double minreal, double maxreal);
    /* get random integer in interval [minint, maxint] set by constructor */
    int randn();
    /* get random integer in interval [minint, maxint]; a lot slower than randn() */
    long randn(long minint, long maxint);
    /* get random double in interval [minreal, maxreal] set by constructor */
    double rand();
    
    static double normpdf(double x, double mu, double sigma);
};

///////////////////////
// RANDOMGENERATOR::RANDOMGENERATOR
// ------------
// STANDARD CONSTRUCTOR FOR THE RANDOM NUMBER GENERATOR
// THIS IS ONLY RECOMMENDED IF ONE WANTS TO ONLY USE RANDN(INT, INT)
// THIS CONSTRUCTOR DOES NOT ALLOW USAGE OF THE OTHER GENERATORS
//
// IN:
//    -
// OUT:
//    -
///////////////////////
RandomGenerator::RandomGenerator() {
  this->gen = std::mt19937(this->rd());
};

///////////////////////
// RANDOMGENERATOR::RANDOMGENERATOR
// ------------
// RECOMMENDED CONSTRUCTOR FOR THE RANDOM NUMBER GENERATOR
//
// IN:
//    MININT    (INT)     ... LOWER BOUND OF INTEGER RANDOM GENERATOR RANDN()
//    MAXINT    (INT)     ... UPPER BOUND OF INTEGER RANDOM GENERATOR RANDN()
//    MINREAL   (DOUBLE)  ... LOWER BOUND OF DOUBLE RANDOM GENERATOR RAND()
//    MAXREAL   (DOUBLE)  ... UPPER BOUND OF DOUBLE RANDOM GENERATOR RAND()
// OUT:
//    -
///////////////////////
RandomGenerator::RandomGenerator(int minint, int maxint, double minreal, double maxreal) {
  this->gen = std::mt19937(this->rd());
  this->disint = std::uniform_int_distribution<>(minint, maxint);
  this->disreal = std::uniform_real_distribution<>(minreal, maxreal);
};

RandomGenerator::~RandomGenerator() {
  /*delete this->gen;
  delete this->disint;
  delete this->disreal;
  delete this->dislong;*/
};

///////////////////////
// RANDOMGENERATOR::RANDN
// ------------
// RETURNS A RANDOM INTEGER IN THE INTERVAL [MININT, MAXINT] SPECIFIED
// IN THE CONSTRUCTOR
//
// IN:
//    -
// OUT:
//    (INT) RANDOM INTEGER IN INTERVAL [MININT, MAXINT]
///////////////////////
int RandomGenerator::randn() {
  return this->disint(this->gen);
};

///////////////////////
// RANDOMGENERATOR::RANDN
// ------------
// RETURNS A RANDOM INTEGER IN THE INTERVAL [MININT, MAXINT]
//
// IN:
//    MININT    (INT) ... LOWER BOUND OF INTEGER RANDOM GENERATOR
//    MAXINT    (INT) ... UPPER BOUND OF INTEGER RANDOM GENERATOR
// OUT:
//    (LONG) RANDOM INTEGER IN INTERVAL [MININT, MAXINT]
///////////////////////
long RandomGenerator::randn(long minint, long maxint) {
  this->dislong = std::uniform_int_distribution<long>(minint, maxint);
  return this->dislong(this->gen);
};

///////////////////////
// RANDOMGENERATOR::RAND
// ------------
// RETURNS A RANDOM DOUBLE IN THE INTERVAL [MINREAL, MAXREAL] SPECIFIED
// IN THE CONSTRUCTOR
//
// IN:
//    -
// OUT:
//    (DOUBLE) RANDOM DOUBLE IN THE INTERVAL [MINREAL, MAXREAL]
///////////////////////
double RandomGenerator::rand() {
  return this->disreal(this->gen);
};

///////////////////////
// RANDOMGENERATOR::NORMPDF
// ------------
// RETURNS THE EVALUATED PDF OF A GAUSS / NORMAL DISTRIBUTION AT GIVEN X
//
// IN:
//    X       (DOUBLE) ... INPUT VARIABLE FOR WHICH PDF SHOULD BE EVALUATED
//    MU      (DOUBLE) ... EXPECTATION VALUE OF GAUSS DISTRIBUTION
//    VAR     (DOUBLE) ... VARIANCE OF GAUSS DISTRIBUTION
// OUT:
//    (DOUBLE) VALUE OF GAUSS / NORMAL PDF AT POINT X
///////////////////////
double RandomGenerator::normpdf(double x, double mu, double var) {
  return 1 / sqrt(2 * M_PI * var) * exp(-(x - mu) * (x - mu) / (2 * var));
};

#endif
