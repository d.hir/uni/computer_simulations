#include "rg.h"
#include "plotutils.h"
#include "vectorutils.h"
#include "timer.h"
#include <vector>
#include <iostream>
#include <cmath>
#include <memory>
#include <string>
#include <fftw3.h>

#define DO_A false  // histograms
#define DO_B false  // average / variance
#define DO_C false  // plot time series
#define DO_D false  // autocorrelation
#define DO_E false  // binning analysis
#define DO_COMPLEXITY_TEST true
#define NUM_STATISTICS 100000
#define DX 15
#define NUM_BARS 25
#define SECURITY_MARGIN 0.05
#define MAX_RHO_LENGTH_PLOT 50
#define PARTIAL_TIME_SERIES_LENGTH 25
#define MAX_LENGTH_LINEAR_BIN_PLOT 500
#define DIFFERENT_LENGTHS_TIMER_TESTS 100
#define STEPSIZE_LENGTHS_TIMER_TESTS 1000
#define NUM_RUNS_PER_LENGTH_TIMER_TESTS 5

using namespace plotutils;
using namespace vectorutils;

std::unique_ptr<RandomGenerator> rg (new RandomGenerator(0, 1, 0.0, 1.0));

void print(std::string str) {
  std::cout << str << std::endl;
}

double pi(const double x, const double xi) {
  return 0.5 * (RandomGenerator::normpdf(x, -xi, 1.0) + 
      RandomGenerator::normpdf(x, xi, 1.0));
}

int nextPow2(int N) {
  return (int) log2(N - 1) + 1;
}

void getAWithoutFFT(std::vector<double> &x, std::vector<double> &A) {
  int N = x.size();
  for (int t = 0; t < N; t++) {
    for (int j = 0; j < N - t - 1; j++) {
      A[j] = x[j] * x[j + t];
    }
  }
}

void getA2(std::vector<double> &x, std::vector<double> &A) {
  int Nraw = x.size();
  int Npad = pow(2, nextPow2(Nraw) + 1);
  if (static_cast<double>(Npad) / Nraw < 2 + SECURITY_MARGIN)
    Npad *= 2;
  x.resize(Npad, 0);
  A.resize(Npad, 0);
  
  fftw_complex* xfft;
  xfft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * Npad);
  
  fftw_plan p = fftw_plan_dft_r2c_1d(Npad, x.data(), xfft, FFTW_ESTIMATE);
  fftw_execute(p);
  
  for (int j = 0; j < Npad; j++) {
    //double re = e[0];
    //e[0] = e[0] * e[0] - e[1] * e[1];
    //e[1] = 2 * re * e[1];
    
    xfft[j][0] = xfft[j][0] * xfft[j][0] + xfft[j][1] * xfft[j][1];
    xfft[j][1] = 0;
    
  }
  
  fftw_plan q = fftw_plan_dft_c2r_1d(Npad, xfft, A.data(), FFTW_ESTIMATE);
  fftw_execute(q);
  
  fftw_destroy_plan(p);
  fftw_destroy_plan(q);
  fftw_cleanup();
  
  for (auto& a : A)
    a /= Npad;
  
  x.resize(Nraw);
  A.resize(Nraw);
}

void getB(const std::vector<double> &x, std::vector<double> &B, 
    bool square = false) {
  B.back() = x[0] * (square ? x[0] : 1);
  for (int j = B.size() - 2; j >= 0; j--)
    B[j] = B[j + 1] + x[B.size() - 1 - j] * (square ? x[B.size() - 1 - j] : 1);
}

void getC(const std::vector<double> &x, std::vector<double> &C, 
    bool square = false) {
  C.back() = x.back() * (square ? x.back() : 1);
  for (int j = C.size() - 2; j >= 0; j--)
    C[j] = C[j + 1] + x[j] * (square ? x[j] : 1);
}

double metropolisStep(const double previous_x, const double xi, int &trials) {
  double proposal_x;
  double p_accept;
  trials++;
  proposal_x = previous_x + (rg->rand() - 0.5) * DX;
  p_accept = std::min(1.0, pi(proposal_x, xi) / pi(previous_x, xi));
  if (rg->rand() < p_accept)
    return proposal_x;
  return previous_x;
}

int getX(std::vector<double> &x, double xi) {
  int trials = 0;
  x[0] = xi;
  for (int k = 1; k < static_cast<int>(x.size()); k++)
    x[k] = metropolisStep(x[k - 1], xi, trials);
  return trials;
}

void getRhoE(std::vector<double> &x, std::vector<double> &rho_E) {
  int N = x.size();
  std::vector<double> A (N);
  getA2(x, A);
  std::vector<double> B (N);
  getB(x, B);
  std::vector<double> C (N);
  getC(x, C);
  std::vector<double> SB (N);
  getB(x, SB, true);
  std::vector<double> SC (N);
  getB(x, SC, true);
  for (int t = 0; t < N; t++) {
    rho_E[t] = (A[t] - B[t] * C[t] / (N - t)) / 
        sqrt((SB[t] - B[t] * B[t] / (N - t)) * (SC[t] - C[t] * C[t] / (N - t)));
  }
}


struct Result {
  std::vector<double> mean;
  std::vector<double> var;
};

void doBinAnalysis(Result& res, const std::vector<int>& k_vec, 
    std::vector<double>& x) {
  struct BinContainer {
    int bin_size_k;
    std::vector<double> partial_means;
    std::vector<double> partial_stddevs;
    BinContainer(int k_) : bin_size_k(k_) {};
  };
  
  std::vector<BinContainer> bin_vec;
  for (auto& k : k_vec)
    bin_vec.push_back(BinContainer(k));
  
  for (auto& b : bin_vec) {
    for (std::vector<double>::iterator it = x.begin(); 
        it < x.end() - b.bin_size_k + 2; it += b.bin_size_k)
      b.partial_means.push_back(mean(it, it + b.bin_size_k));
    res.mean.push_back(mean(b.partial_means));
    res.var.push_back(var(b.partial_means) / b.partial_means.size());
  }
}

void set2(double xi, int num_vals) {
  std::string xi_str = str(boost::format("%.1f") % xi);
  std::string full_xi_str = "{/Symbol x} = " + xi_str + ": ";
  print("############ xi = " + xi_str + " ############");
  std::vector<double> x(num_vals);
  int trials = getX(x, xi);  
  std::cout << "Rejection Rate: " << 1 - (double) num_vals / trials 
      << std::endl;
      
  if (DO_A) {
    Histogram h (NUM_BARS, -xi - 5, xi + 5);
    h.setData(x);
    h.plot(3, "1/sqrt(2 * pi) / 2 * (exp(-(@x - " + std::to_string(xi) + 
        ") ** 2 / 2) + exp(-(@x - " + std::to_string(-xi) + ") ** 2 / 2))");
    h.xlabel("{/:Italics x}");
    h.ylabel("{/:Italics p(x)}");
    h.title(full_xi_str + "Histogram");
  }
  
  if (DO_B) {
    std::cout << "Mean: " << mean(x) << std::endl;
    std::cout << "Var:  " << var(x)  << std::endl;
  }
  
  if (DO_C) {
    plot(x, "x_i");
    title(full_xi_str + "Complete Time Series {/:Italic x_i}");
    ylabel("{/:Italic x_i}");
    xlabel("Iteration");
    
    std::vector<double> xpartial (x.begin(), 
        std::min(x.begin() + PARTIAL_TIME_SERIES_LENGTH, x.end()));
    plot(xpartial, "x_i");
    title(full_xi_str + "Partial Time Series {/:Italic x_i}");
    ylabel("{/:Italic x_i}");
    xlabel("Iteration");
  }

  if (DO_D) {
    std::vector<double> rho_E(num_vals);
    getRhoE(x, rho_E);
    int shrink_size = std::min(num_vals, MAX_RHO_LENGTH_PLOT);
    rho_E.resize(shrink_size);

    
    std::vector<double> log_rho;
    std::for_each(rho_E.begin(), rho_E.end(), 
        [&log_rho](double rho){log_rho.push_back(log(rho));});
    std::vector<double> t_vec (log_rho.size());
    colon(t_vec, 0.0);
    std::vector<double> p = polyfit(t_vec, log_rho, 1);
    std::vector<double> fit;
    polyval(fit, p, t_vec);
    std::for_each(fit.begin(), fit.end(), [](double& e){e = exp(e);});
    plot(rho_E, "{/Symbol r}_E({/:Italic t})", "y");
    title(full_xi_str + "autocorrelation {/Symbol r}_E({/:Italic t})");
    xlabel("{/:Italic t}");
    ylabel("{/Symbol r}_E({/:Italic t})");
    
    hold_on();
    plot(fit, "linear fit", "", "lines");
    hold_off();
    
    print("Inverse slope of ln(rho_E): " + std::to_string(1.0 / p[1]));
  }
  
  if (DO_E) {
    Result log2bins;
    int max_pow_k = nextPow2(num_vals) - 2;
    std::vector<int> k_log_vec (max_pow_k + 1);
    logspace(k_log_vec, 0, max_pow_k, 2);
    doBinAnalysis(log2bins, k_log_vec, x);
    plot(k_log_vec, log2bins.var, "{/Symbol s}^2", "x");
    xlabel("{/:Italic k}");
    ylabel("{/Symbol s}^2");
    title(full_xi_str + "Binning Analysis: {/Symbol s}^2 with logarithmically" + 
        " spaced {/:Italic k} values");
    
    Result lin_bins;
    int max_lin_k = std::min(num_vals, MAX_LENGTH_LINEAR_BIN_PLOT);
    std::vector<int> k_lin_vec (max_lin_k);
    linspace(k_lin_vec, 1, max_lin_k);
    doBinAnalysis(lin_bins, k_lin_vec, x);
    plot(k_lin_vec, lin_bins.var, "{/Symbol s}^2");
    xlabel("{/:Italic k}");
    ylabel("{/Symbol s}^2");
    title(full_xi_str + "Binning Analysis: {/Symbol s}^2 with linearly" + 
        " spaced {/:Italic k} values");
  }
}

int main() {
  std::vector<double> xi_vec {0.0, 2.0, 6.0};
  for (auto& xi : xi_vec)
    set2(xi, NUM_STATISTICS);
  
  if (DO_COMPLEXITY_TEST) {
    double xi_time = 1.0;
    std::vector<int> timer_length (DIFFERENT_LENGTHS_TIMER_TESTS);
    linspace(timer_length, STEPSIZE_LENGTHS_TIMER_TESTS * 
        DIFFERENT_LENGTHS_TIMER_TESTS, STEPSIZE_LENGTHS_TIMER_TESTS);
    std::vector<double> time_means;
    std::vector<std::vector<double>> x_time(NUM_RUNS_PER_LENGTH_TIMER_TESTS);
    for (auto& x_time_vector : x_time) {
      x_time_vector = std::vector<double> (timer_length[0]);
      getX(x_time_vector, xi_time);
    }
    for (auto& tl : timer_length) {
      std::vector<double> time_individual;
      for (auto& x_time_vector : x_time) {
        if (!(tl == timer_length[0]))
          x_time_vector.resize(tl);
        Timer timer;
        std::vector<double> rho_E(tl);
        timer.start();
        getRhoE(x_time_vector, rho_E);
        timer.end();
        time_individual.push_back(timer.get());
      }
      time_means.push_back(mean(time_individual));
    }
    
    std::vector<double> NlnN;
    std::for_each(timer_length.begin(), timer_length.end(), [&NlnN](int l) {
        NlnN.push_back(l * log(l));});
    std::vector<double> p = polyfit(NlnN, time_means, 1);
    std::vector<double> fit;
    polyval(fit, p, NlnN);
    
    plot(timer_length, time_means, "measurement {/:Italic t(N)}");
    xlabel("{/:Italic N}");
    ylabel("{/:Italic t} / ns");
    title("Complexity / Time measurements for autocorrelation");
    _set("key", "left top", "", "");
    
    hold_on();
    plot(timer_length, fit, "N*ln(N) fit", "", "lines");
    hold_off();
  }
  
  return 0;
}
