////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  RG.H
////  
////  CLASS CREATING (BETTER) RANDOM NUMBERS
////  PROVIDES MATLAB-ESK FRONTEND FOR STANDARD C++ <RANDOM>
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef RG_H
#define RG_H

/* provides random numbers */
#include <random>
#include <cmath>

class RandomGenerator {
  private:
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> disint;
    std::uniform_int_distribution<long> dislong;
    std::uniform_real_distribution<double> disreal;
    std::normal_distribution<double> disnormal;
    bool normal_distribution_set = false;
    
  public:
    /* standard constructor */
    RandomGenerator();
    ~RandomGenerator();
    /* full constructor */
    RandomGenerator(int minint, int maxint, double minreal, double maxreal);
    /* get random integer in interval [minint, maxint] set by constructor */
    int randi();
    /* get random integer in interval [minint, maxint]; slower than randn() */
    long randi(long minint, long maxint);
    /* get normally distributed random number with given mean and stdev */
    double randn(double mean, double stdev);
    /* set mean and stdev of normal distribution */
    void setupRandn(double mean, double stdev);
    /* get normally distributed random with params set by setupRandn */
    double randn();
    /* get random double in interval [minreal, maxreal] set by constructor */
    double rand();
    
    static double normpdf(double x, double mu, double sigma);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RANDOMGENERATOR
// --------------------------------
// STANDARD CONSTRUCTOR FOR THE RANDOM NUMBER GENERATOR
// THIS INITIALIZES THE FLOATING POINT GENERATOR TO [0, 1]
// AND THE INTEGER GENERATOR TO [0, 10]
//
// IN:
//    -
// OUT:
//    -
////////////////////////////////////////////////////////////////////////////////
RandomGenerator::RandomGenerator() {
  this->gen = std::mt19937(this->rd());
  this->disreal = std::uniform_real_distribution<>(0.0, 1.0);
  this->disint = std::uniform_int_distribution<>(0, 10);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RANDOMGENERATOR
// --------------------------------
// RECOMMENDED CONSTRUCTOR FOR THE RANDOM NUMBER GENERATOR
//
// IN:
//    MININT    (INT)     ... LOWER BOUND OF INTEGER RANDOM GENERATOR RANDN()
//    MAXINT    (INT)     ... UPPER BOUND OF INTEGER RANDOM GENERATOR RANDN()
//    MINREAL   (DOUBLE)  ... LOWER BOUND OF DOUBLE RANDOM GENERATOR RAND()
//    MAXREAL   (DOUBLE)  ... UPPER BOUND OF DOUBLE RANDOM GENERATOR RAND()
// OUT:
//    -
////////////////////////////////////////////////////////////////////////////////
RandomGenerator::RandomGenerator(int minint, int maxint, double minreal, 
    double maxreal) {
  this->gen = std::mt19937(this->rd());
  this->disint = std::uniform_int_distribution<>(minint, maxint);
  this->disreal = std::uniform_real_distribution<>(minreal, maxreal);
};

RandomGenerator::~RandomGenerator() {
  /*delete this->gen;
  delete this->disint;
  delete this->disreal;
  delete this->dislong;*/
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RANDI
// ----------------------
// RETURNS A RANDOM INTEGER IN THE INTERVAL [MININT, MAXINT] SPECIFIED
// IN THE CONSTRUCTOR
//
// IN:
//    -
// OUT:
//    (INT) RANDOM INTEGER IN INTERVAL [MININT, MAXINT]
////////////////////////////////////////////////////////////////////////////////
int RandomGenerator::randi() {
  return this->disint(this->gen);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RANDI
// ----------------------
// RETURNS A RANDOM INTEGER IN THE INTERVAL [MININT, MAXINT]
//
// IN:
//    MININT    (INT) ... LOWER BOUND OF INTEGER RANDOM GENERATOR
//    MAXINT    (INT) ... UPPER BOUND OF INTEGER RANDOM GENERATOR
// OUT:
//    (LONG) RANDOM INTEGER IN INTERVAL [MININT, MAXINT]
////////////////////////////////////////////////////////////////////////////////
long RandomGenerator::randi(long minint, long maxint) {
  this->dislong = std::uniform_int_distribution<long>(minint, maxint);
  return this->dislong(this->gen);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RANDN
// ----------------------
// RETURNS A NORMAL (GAUSSIAN) DISTRIBUTED DOUBLE
//
// IN:
//    MEAN  (DOUBLE) ... MEAN VALUE OF GAUSS DISTRIBUTION
//    STDEV (DOUBLE) ... STANDARD DEVIATION OF GAUSS DISTRIBUTION
// OUT:
//    (DOUBLE) GAUSSIAN DISTRIBUTED RANDOM NUMBER
////////////////////////////////////////////////////////////////////////////////
double RandomGenerator::randn(double mean, double stdev) {
  this->disnormal = std::normal_distribution<double>(mean, stdev);
  return this->disnormal(this->gen);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::SETUPRANDN
// ---------------------------
// SETS UP A NORMAL (GAUSSIAN) DISTRIBUTION TO BE ABLE TO USE RANDN()
//
// IN:
//    MEAN  (DOUBLE) ... MEAN VALUE OF GAUSS DISTRIBUTION
//    STDEV (DOUBLE) ... STANDARD DEVIATION OF GAUSS DISTRIBUTION
// OUT:
//    -
////////////////////////////////////////////////////////////////////////////////
void RandomGenerator::setupRandn(double mean, double stdev) {
  this->disnormal = std::normal_distribution<double>(mean, stdev);
  this->normal_distribution_set = true;
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RANDN
// ----------------------
// RETURNS A NORMAL (GAUSSIAN) DISTRIBUTED DOUBLE.
// REQUIRES SETUPRANDN(MEAN, STDEV) TO BE CALLED FIRST.
// USES MEAN AND STDEV SET BY SETUPRANDN
//
// IN:
//    -
// OUT:
//    (DOUBLE) GAUSSIAN DISTRIBUTED RANDOM NUMBER
////////////////////////////////////////////////////////////////////////////////
double RandomGenerator::randn() {
  return this->disnormal(this->gen);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::RAND
// ---------------------
// RETURNS A RANDOM DOUBLE IN THE INTERVAL [MINREAL, MAXREAL] SPECIFIED
// IN THE CONSTRUCTOR
//
// IN:
//    -
// OUT:
//    (DOUBLE) RANDOM DOUBLE IN THE INTERVAL [MINREAL, MAXREAL]
////////////////////////////////////////////////////////////////////////////////
double RandomGenerator::rand() {
  return this->disreal(this->gen);
};

////////////////////////////////////////////////////////////////////////////////
// RANDOMGENERATOR::NORMPDF
// ------------------------
// RETURNS THE EVALUATED PDF OF A GAUSS / NORMAL DISTRIBUTION AT GIVEN X
//
// IN:
//    X       (DOUBLE) ... INPUT VARIABLE FOR WHICH PDF SHOULD BE EVALUATED
//    MU      (DOUBLE) ... EXPECTATION VALUE OF GAUSS DISTRIBUTION
//    VAR     (DOUBLE) ... VARIANCE OF GAUSS DISTRIBUTION
// OUT:
//    (DOUBLE) VALUE OF GAUSS / NORMAL PDF AT POINT X
////////////////////////////////////////////////////////////////////////////////
double RandomGenerator::normpdf(double x, double mu, double var) {
  return 1 / sqrt(2 * M_PI * var) * exp(-(x - mu) * (x - mu) / (2 * var));
};

#endif
