#include <iostream>
#include <vector>
#include <fstream>
#include "particle.h"
#include "plotutils.h"

using namespace plotutils;

class MeasurementDevice {
  private:
    std::vector<double> heights;
    std::vector<double> velocities;
    double max_height = 0;
    double max_velocity = 0;
  
  public:
    static int HEIGHT_DIR;
    MeasurementDevice() {};
    
    void measureHeight(Particle& p) {
      double new_height = p.pos.v[MeasurementDevice::HEIGHT_DIR];
      this->heights.push_back(new_height);
      if (new_height > this->max_height)
        this->max_height = new_height;
    };
    
    void measureVelocity(Particle& p) {
      PVector<> vel_zero(p.dims);
      double vel = PVector<>::distance(vel_zero, p.vel);
      this->velocities.push_back(vel);
      if (vel > this->max_velocity)
        this->max_velocity = vel;
    };
    
    void makeHistograms() {
      std::ofstream file;
      file.open("velocities.csv");
      for (auto& v : this->velocities)
        file << v << "\n";
      file.close();
      file.open("heights.csv");
      for (auto& v : this->heights)
        file << v << "\n";
      file.close();
      std::system("python3 analysis.py");
    };
};

int MeasurementDevice::HEIGHT_DIR = 1;
