#ifndef MOLECULARDYNAMICS_H
#define MOLECULARDYNAMICS_H

#include <vector>
#include <iostream>
#include <cassert>
#include <string>
#include "particle.h"
#include "measurementdevice.h"
#include "plotutils.h"
#include "vectorutils.h"

using namespace plotutils;
using namespace vectorutils;

class MolecularDynamics {
  private:
    int GRAVITY_DIM = 1;
    std::vector<Particle> particles;
    Figure<> fig_box;
    Figure<> fig_energy;
    int dim = 2;
    double sigma = 1;
    double epsilon = 1;
    double tau = 1e-4;
    double box_length = 5;
    double g = 0.0;
    int N;
    bool first_plot_box = true;
    bool first_plot_energy = true;
    std::vector<double> energy;
    std::vector<double> kinetic_energy;
    std::vector<double> gravitational_energy;
    std::vector<double> lennard_jones_energy;
    MeasurementDevice measurement_device;
  
  public:
    static int NSKIP;
    static int NMEAS;
    static int NEQUI;
    
    MolecularDynamics(int N_) : N{N_} {
      std::cout << "Initializing..." << std::endl;
      for (int j = 0; j < this->N; j++)
        this->particles.push_back(Particle(2, j, sigma, box_length));
    };
    
    void plotMolecules() {
      if (this->dim == 2) {
        std::vector<double> pos_x;
        std::vector<double> pos_y;
        for (auto& p : this->particles) {
          pos_x.push_back(p.pos.x());
          pos_y.push_back(p.pos.y());
        }
        this->fig_box.overrideWithNextPlot();
        this->fig_box.plot(pos_x, pos_y);
        if (this->first_plot_box) {
          this->fig_box.title("Box with Particles");
          this->fig_box.xlabel("x / {/Symbol s}");
          this->fig_box.ylabel("y / {/Symbol s}");
          this->first_plot_box = false;
        }
        this->fig_box.flush();
      }
    };
    
    void plotTotalEnergy() {
      this->fig_energy.overrideWithNextPlot();
      this->fig_energy.plot(this->energy, "Total Energy");
      //this->fig_energy.plot(this->kinetic_energy, "Kinetic Energy");
      //this->fig_energy.plot(this->gravitational_energy, "Gravitational Energy");
      //this->fig_energy.plot(this->lennard_jones_energy, 
      //    "Lennard Jones Potential");
      if (this->first_plot_energy) {
        this->fig_energy.title("Total Energy of System");
        this->fig_energy.xlabel("t / (" + 
            std::to_string(MolecularDynamics::NSKIP) + " {/Symbol t})");
        this->fig_energy.ylabel("E_{tot} / {/Symbol e}");
        this->first_plot_energy = false;
      }
      this->fig_energy.flush();
    };
    
    void simulate() {
      std::cout << "Thermalizing..." << std::endl;
      for (int j = 0; j < MolecularDynamics::NEQUI; j++)
        this->doTimestep();
      std::cout << "Creating figures..." << std::endl;
      this->plotMolecules();
      std::cout << "Measuring..." << std::endl;
      this->measureEnergy();
      for (int j = 0; j < MolecularDynamics::NMEAS; j++) {
        for (int k = 0; k < MolecularDynamics::NSKIP; k++)
          this->doTimestep();
        this->plotMolecules();
        this->measureEnergy();
        for (auto& p : this->particles) {
          this->measurement_device.measureHeight(p);
          this->measurement_device.measureVelocity(p);
        }
        this->plotTotalEnergy();
      }
      std::cout << "Generating Histograms..." << std::endl;
      this->measurement_device.makeHistograms();
      std::cout << "Done" << std::endl;
    };
    
    static PVector<double> getLennardJonesForce(Particle& p1, Particle& p2) {
      PVector<double> f = p1.pos;
      f.subtract(p2.pos);
      double r = PVector<>::distance(p1.pos, p2.pos);
      f *= 48 / r / r * (1 / pow(r, 12) - 1 / (2 * pow(r, 6)));
      return f;
    };
    
    PVector<double> getTotalForce(Particle& p) {
      PVector<double> f (this->dim);
      for (auto& pi : this->particles) {
        if (p == pi)
          continue;
        f.add(this->getLennardJonesForce(p, pi));
      }
      f.v[GRAVITY_DIM] -= this->g * p.mass();
      return f;
    };
    
    void doTimestep() {
      for (auto& p : this->particles) {
        PVector<double> f = this->getTotalForce(p);
        //f.print();
        p.pos += p.vel * this->tau + f * this->tau * this->tau * 0.5;
        //std::cout << "############" << std::endl;
        //p.pos.print();
        p.vel += (f + this->getTotalForce(p)) * this->tau * 0.5;
      }
      this->checkReflections();
    };
    
    void checkReflections() {
      for (auto& p : this->particles) {
        p.checkReflection(0, Particle::dirs::both);
        p.checkReflection(1, Particle::dirs::negative);
      }
    };
    
    void print() {
      std::cout << " ------ " << std::endl;
      for (auto& p : this->particles)
        p.print();    
    };
    
    void measureEnergy() {
      double U_lennard_jones = 0.0;
      for (auto& p : this->particles) {
        for (auto& q : this->particles) {
          if (p == q)
            continue;
          double sig_r = this->sigma / PVector<>::distance(p.pos, q.pos);
          U_lennard_jones += pow(sig_r, 12) - pow(sig_r, 6);
        }
      }
      U_lennard_jones *= 4 * this->epsilon;
      
      this->lennard_jones_energy.push_back(U_lennard_jones);
      
      double U_gravity = 0.0;
      for (auto& p : this->particles) {
        U_gravity += p.pos.v[GRAVITY_DIM] * p.mass();
      }
      U_gravity *= this->g;
      
      this->gravitational_energy.push_back(U_gravity);
      
      double T = 0.0;
      for (auto& p : this->particles)
        T += p.mass() * p.vel_squared() * 0.5;
      
      this->kinetic_energy.push_back(T);
      
      this->energy.push_back(U_lennard_jones + U_gravity + T);
    };
};

int MolecularDynamics::NSKIP = 10;
int MolecularDynamics::NMEAS = 1000;
int MolecularDynamics::NEQUI = 10000;
#endif
