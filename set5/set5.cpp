#include "moleculardynamics.h"
#include <vector>
#include <iostream>

int main () {
  MolecularDynamics md (50);
  md.simulate();

  return 0;
}
